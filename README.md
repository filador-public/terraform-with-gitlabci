# Terraform with GitLab CI

A complete CI/CD pipeline for deployment with Terraform.

You can refer to this French article to know more about the code : [Une CI/CD prête à l'emploi pour Terraform avec GitLab CI](https://blog.filador.fr/une-ci-cd-prete-a-lemploi-pour-terraform-avec-gitlab-ci/)
