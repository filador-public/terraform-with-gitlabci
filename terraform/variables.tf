variable "region" {
  type        = string
  description = "The Google Cloud region"
}

variable "zone" {
  type        = string
  description = "The Google Cloud zone"
}

variable "machine_type" {
  type        = string
  description = "The compute instance machine type"
}

variable "image" {
  type        = string
  description = "The image name for compute engine instance"
}

variable "disk_size" {
  type        = string
  description = "The disk boot size for compute engine instance"
}

variable "instance_name" {
  type        = string
  description = "The instance name"
}

variable "project_id" {
  type        = string
  description = "The Google Cloud Project ID"
}

variable "network_name" {
  type        = string
  description = "The VPC network name"
}

variable "subnet_name" {
  type        = string
  description = "The subnetwork name"
}

variable "subnet_cidr" {
  type        = string
  description = "The subnetwork cidr range"
}

variable "uptime_check_url" {
  type        = string
  description = "The service url to watch"
}

variable "notification_email" {
  type        = string
  description = "Personnal email to receive monitoring notifications"
}