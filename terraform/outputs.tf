output "project_id" {
  value       = var.project_id
  description = "The project ID"
}

output "external_ip" {
  value       = google_compute_instance.external_services.network_interface[0].access_config[0].nat_ip
  description = "The external IP address of VM"
}