resource "google_storage_bucket" "backup" {
  #checkov:skip=CKV_GCP_62:Disable versioning for Google Cloud Free Tier
  #checkov:skip=CKV_GCP_78:Disable access log for Google Cloud Free Tier
  project = var.project_id

  name          = "${var.project_id}-backup"
  location      = var.region
  storage_class = "REGIONAL"

  uniform_bucket_level_access = true

  public_access_prevention = "enforced"
  force_destroy            = false

  lifecycle_rule {
    condition {
      age = 10
    }
    action {
      type = "Delete"
    }
  }
}
