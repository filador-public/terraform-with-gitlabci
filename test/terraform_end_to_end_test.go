package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/gcp"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformEndToEnd(t *testing.T) {

	terraformOptionsGoogleCloudFreeTier := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../terraform",
		VarFiles: []string{
			"./configurations/test.tfvars",
		},
		BackendConfig: map[string]interface{}{
			"bucket": "", // Update bucket name
			"prefix": "google-cloud-free-tier/test",
		},
		Reconfigure: true,
	})

	defer terraform.Destroy(t, terraformOptionsGoogleCloudFreeTier)

	terraform.InitAndApply(t, terraformOptionsGoogleCloudFreeTier)

	// Get project ID from outputs
	projectId := terraform.Output(t, terraformOptionsGoogleCloudFreeTier, "project_id")

	/* 
		Do some checks
	*/
	expectedInstanceName := "ci-external-services"
	expectedMachineType := "e2-micro"
	expectedVMStatus := "RUNNING"

	instance := gcp.FetchInstance(t, projectId, expectedInstanceName)

	// Check machine type
	assert.Contains(t, instance.Instance.MachineType, expectedMachineType)
	// Check status
	assert.Equal(t, expectedVMStatus, instance.Instance.Status)
}
